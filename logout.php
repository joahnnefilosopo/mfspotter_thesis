<?php

	session_start();
	
	if((isset($_SESSION['username'])) && (isset($_SESSION['password'])))
	{
		// remove all session variables
		session_unset(); 
		
		redirect("index.php");
		
	}


	function redirect($url)
      {
        echo '<META HTTP-EQUIV=Refresh CONTENT="1; URL='.$url.'">';
        die();
      }
		
?>