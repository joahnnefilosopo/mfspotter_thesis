<?php

$month = $_POST['emonth'];
include('config.php');

//$query = "SELECT facility.facilityID, facility.facilityName, facility.address, (AVG((a.rating)) + (SELECT AVG(b.rating)/2)) /2 as Rating FROM rating a, rating b, facility  WHERE a.facilityID = facility.facilityID AND b.facilityID = facility.facilityID AND a.dateRated >= DATE_SUB('".$month."',INTERVAL 1 MONTH) GROUP BY facility.facilityID  ORDER BY Rating  DESC LIMIT 10";

$query = "SELECT facility.facilityID, facility.facilityName, facility.address, (AVG((a.rating)) + (SELECT AVG(b.rating)/2)) /2 as Rating 
FROM rating a, rating b, facility
WHERE a.facilityID = facility.facilityID 
AND b.facilityID = facility.facilityID 
AND b.dateRated >= DATE_SUB('".$month."',INTERVAL 1 MONTH) 
AND a.dateRated >= DATE_SUB('".$month."',INTERVAL 1 MONTH) 
GROUP BY facility.facilityID 
ORDER BY Rating DESC LIMIT 10";

$result = mysqli_query($connect, $query); 
$i = 1;
echo "<div class='table-responsive'>
              <table class='table no-margin'>
                <thead>
                  <tr>
                    <th>Rank</th>
                    <th>Facility Name</th>
                    <th>Facility Address</th>
                    <th>Rating</th>
                  </tr>
                </thead>
                <tbody>
                 ";
    if(mysqli_num_rows($result) > 0)  
     {  
          while($row = mysqli_fetch_array($result))
          { ?>
           
          <tr>
              <td>#<?php echo $i; ?></td>
              <td><a href ="insertView.php?id=<?php echo $row['facilityID'];?>" ><?php echo $row['facilityName'];?></a></td>
              <td><?php echo $row['address'];?></td>
              <td><?php echo number_format($row['Rating'],2); $i++ ?></td>
          </tr>
            
           <?php

          }
        

     }  
     else  
     {  
          echo 'Data Not Found';  
     }





?>