
<?php 
   function countUsers(){

    include('config.php');
    $query = "SELECT count(*) AS NumberOfUsers FROM user WHERE userType = 'User'";
    $result = mysqli_query($connect, $query);

    if(mysqli_num_rows($result) > 0)  
    {  
      while($row = mysqli_fetch_array($result)) 
      {

        return $row['NumberOfUsers'];
      }
    }
    else
    {
      return 0;
    }
  }

  function countFacilities(){

    include('config.php');
    $query = "SELECT count(*) AS NumberOfFacilities FROM facility";
    $result = mysqli_query($connect, $query);

    if(mysqli_num_rows($result) > 0)  
    {  
      while($row = mysqli_fetch_array($result)) 
      {

        return $row['NumberOfFacilities'];
      }
    }
    else
    {
      return 0;
    }
  }



  function countUsersWhoRated(){
    include('config.php');
    $query = "SELECT count(*)/4 AS facility FROM rating, user WHERE user.userID=rating.userID";
    $result = mysqli_query($connect, $query);

    if(mysqli_num_rows($result) > 0)  
    {  
      while($row = mysqli_fetch_array($result)) 
      {

        return $row['facility'];
      }
    }
    else
    {
      return 0;
    }
  }


 function countRatedFacility(){
    include('config.php');
    $query = "SELECT count(DISTINCT rating.facilityID) AS facility FROM rating, facility WHERE rating.facilityID = facility.facilityID";
    $result = mysqli_query($connect, $query);

    if(mysqli_num_rows($result) > 0)  
    {  
      while($row = mysqli_fetch_array($result)) 
      {

        return $row['facility'];
      }
    }
    else
    {
      return 0;
    }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>MFSpotter | Main Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">




</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="/mfspotter/index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>MFS</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>MF</b>Spotter</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <!-- RIGHT SIDE TOP -->
      <?php
        include("config.php");
        session_start();
        include("header.php");
      ?>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
      
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
     
        </li>
   
       
       <?php 
          if(!empty($_SESSION))
          {
            
          }
          else
          {
            echo '
            <li class="treeview">
              <a href="/mfspotter/registration_facility2.php">
                <i class="fa fa-pie-chart"></i>
                <span>Register as Facility</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
            </li>

            <li class="treeview">
              <a href="/mfspotter/registration_user.php">
                <i class="fa fa-laptop"></i>
                <span>Register as User</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
            </li>';
          }
       ?>
        

        <li class="treeview">
          <a href="/mfspotter/searchBy.php">
            <i class="fa fa-edit"></i> <span>View Facilities</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
       
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Welcome!
      </h1>
    
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo countRatedFacility(); ?></h3>

              <p>Rated Facilities</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo number_format(countUsersWhoRated(),0); ?></h3>

              <p>Users Who Rated</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo countUsers();?></h3>

              <p>Registered Users</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo countFacilities();?></h3>

              <p>Registered Facilities</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <div class="box box-info">
          <div class="box-header with-border">
          <h3 class="box-title">Top Rated Facilities</h3>

            <div class="box-tools">
                <select class="form-control" id="checkmonth">
                      <option value="2016-09-01">September</option>
                      <option value="2016-10-01">October</option>
                      <option value="2016-11-01">November</option>
                      <option value="2016-12-01">December</option>
                      <option value="2017-01-01">January</option>
                      <option value="2017-02-01">February</option>
                </select>
                <button type="button" class="btn btn-outline" onclick="viewTopRated()">View</button>
                 
              
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body" id="result">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>Rank</th>
                    <th>Facility Name</th>
                    <th>Facility Address</th>
                    <th>Rating</th>
                  </tr>
                </thead>
                <tbody>
                 



                <?php 
                  include('config.php');
                  $query = "SELECT facility.facilityID, facility.facilityName, facility.address, (AVG((a.rating)) + (SELECT AVG(b.rating)/2)) /2 as Rating FROM rating a, rating b, facility  WHERE a.facilityID = facility.facilityID AND b.facilityID = facility.facilityID AND a.dateRated >= DATE_SUB('2016-11-01',INTERVAL 1 MONTH) GROUP BY facility.facilityID  ORDER BY Rating  DESC LIMIT 10";
                  $result = mysqli_query($connect, $query); 
                  $i = 1;
                  if(mysqli_num_rows($result) > 0)  
                  {  
                     while($row = mysqli_fetch_array($result))
                      {
                ?>

                  <tr>
                    <td>#<?php echo $i; ?></td>
                    <td><a href ="insertView.php?id=<?php echo $row['facilityID'];?>" ><?php echo $row['facilityName'];?></a></td>
                    <td><?php echo $row['address'];?></td>
                    <td><?php echo number_format($row['Rating'],2);?></td>
                   
                  </tr>

                  <?php $i++;}  }?>
         


                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix">
          </div>
          <!-- /.box-footer -->
        </div>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
  
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.4
    </div>
    <strong><a href="#">MFSpotter</a>.</strong> Helping Displaced People.
  </footer>
</div>
<!-- ./wrapper -->




<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script type="text/javascript">
      
      function viewTopRated() {
        //  var month = $("#checkMonth").val();
          var month = document.getElementById("checkmonth").value;
 
       
          
          $.ajax({  
                         url:"index_table.php",  
                         method:"post",  
                         data:{emonth: month},  
                         dataType:"text",  
                         success:function(data)  
                         {  
                              $('#result').html(data);  
                         }  
                    });  
        }
    </script>
</body>
</html>
