<?php
  
  if(!(isset($_SESSION['username'])) && !(isset($_SESSION['password'])))
  {
    //echo "<script>alert('Not Logged in!')</script>";
    //redirect('login.php');

    echo '<div class="collapse navbar-collapse pull-right" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="#">About</a></li>
            <li><a href="login.php">Log In</a></li>
          </ul>
          
        </div>';
      
  }
    
  else
  {

    $type = $_SESSION['usertype'];
    $user = $_SESSION["user_id"];

    echo '<div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li><a href="#">About</a></li>
          <!-- User Account: style can be found in dropdown.less -->';


    if($type == "staff"){
      //Selecting the facility of the staff
      $selectQuery = "SELECT f.facilityID FROM facility f, facilityhasstaff fs, user u WHERE f.facilityID = fs.facilityID AND u.userID = fs.userID AND u.userID = '$user'";
      $SelectSql = @mysqli_query($connect, $selectQuery);

      
      $roww = mysqli_fetch_array($SelectSql);

      echo ' <li><a href="staff_facilityProfile.php?id='. $roww['facilityID'] .'">Manage Medical Facility</a></li>';
    }
    

    echo '<li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="dist/img/profpic/'.$_SESSION["profilePicture"].'.jpg" class="user-image" alt="User Image">
            <span class="hidden-xs">'.$_SESSION["firstname"].' '.$_SESSION["lastname"] .'</span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="dist/img/profpic/'.$_SESSION["profilePicture"].'.jpg" class="img-circle" alt="User Image">

              <p>
                '.$_SESSION["firstname"].' '.$_SESSION["lastname"] .'
                <small>'. $_SESSION["usertype"] .'</small>
              </p>
            </li>

            <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="userProfile.php?id='. $_SESSION['user_id'] .'" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          
        </ul>
      </div>';            
  }
    
  
  function redirect($url)
  {
    echo '<META HTTP-EQUIV=Refresh CONTENT="1; URL='.$url.'">';
    die();
  }

  
?>

   